<?php

namespace App;

use Jenssegers\Model\Model;

class CamposExtraidos extends Model
{
    use \Illuminate\Database\Eloquent\Concerns\HasAttributes;
    public $incrementing = false;

    public function __construct(array $attributes = [])
    {
        //$this->bootIfNotBooted();
        //$this->initializeTraits();
        parent::__construct($attributes);
        $this->syncOriginal();
    }

    public static function find($idContrato){
        //$arreglo_final = LlamadoDatosComoExtraccion::get();
        $instance = new static;
        $arreglo_final = ['Objeto' => 'hola pruebas', 'EntidadContratante' => 'Pruebas 2'];
        return $instance->setRawAttributes($arreglo_final, true);
    }

    public function save(){
        return true;
    }

    public function usesTimestamps(){
        return false;
    }

    public function getCreatedAtColumn(){
        return 'created_at';
    }

    public function getUpdatedAtColumn(){
        return 'updated_at';
    }

    public function getIncrementing(){
        return $this->incrementing;
    }
}
