<?php

namespace App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;


final class DesayunoMarzoType extends Enum implements LocalizedEnum
{
    const Sushi = 0;
    const Creeps = 1;
    const HuevoyArepa = 2;
    const Hamburguesa = 3;
    const ArepaChocolo = 'SI';

}
