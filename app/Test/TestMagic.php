<?php

namespace App\Test;

use Illuminate\Support\Traits\Macroable;

class TestMagic {

    use Macroable {
        Macroable::__call as macroCall;
    }

    public function success () {
        return function ($message){
            return ['success' => true, 'message' => $message];
        };
    }

}