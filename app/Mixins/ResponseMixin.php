<?php

namespace App\Mixins;

use Illuminate\Support\Facades\Response;

class ResponseMixin {

    public function success () {
        return function ($message){
            return ['success' => true, 'message' => $message];
        };
    }

    /*public function fail ($message) {
        return ['success' => false, 'message' => $message];
    }*/

}