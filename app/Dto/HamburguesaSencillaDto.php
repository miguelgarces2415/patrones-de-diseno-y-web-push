<?php 

namespace App\Dto;

use Spatie\DataTransferObject\DataTransferObject;

class HamburguesaSencillaDto extends DataTransferObject
{
    /**
     * Built in types:
     */
    public string $pan;

    /**
     * Imported class or fully qualified class name:
     */
    public string $proteina;

}