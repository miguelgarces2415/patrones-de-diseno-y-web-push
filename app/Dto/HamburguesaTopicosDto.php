<?php 

namespace App\Dto;

use Spatie\DataTransferObject\DataTransferObject;

class HamburguesaTopicosDto extends DataTransferObject
{
  
    public string $nombre;
    public string $valor;

}