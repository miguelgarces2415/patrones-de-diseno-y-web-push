<?php 

namespace App\Dto;

use Spatie\DataTransferObject\DataTransferObject;

class HamburguesaDto extends DataTransferObject
{
    /**
     * Built in types:
     */
    public string $pan;

    /**
     * Imported class or fully qualified class name:
     */
    public string $proteina;

    /** @var \App\Dto\HamburguesaTopicosDto[] */
    public array $topicos;

}