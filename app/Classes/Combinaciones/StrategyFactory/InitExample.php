<?php
namespace App\Classes\Combinaciones\StrategyFactory;

use App\Classes\Combinaciones\StrategyFactory\Reporte;
use App\Classes\Combinaciones\StrategyFactory\Factories\EstrategiaFactory;

class InitExample {

    public function __construct(array $datos = ['celdas' => true]) {
        $this->init($datos);
    }

    /*
    * Es el cliente quien decide que estrategia utilizar
    */
    public function init(array $datos): void {
        $reporte = new Reporte($datos);
        $reporte->setEstrategia(EstrategiaFactory::getReporteEstrategia($datos));
        $reporte->ejecutar();
    }
}