<?php 

namespace App\Classes\Combinaciones\StrategyFactory\Interfaces;

interface EstrategiasInterface {
    /*
    * Obligamos a que todas las estrategias generen un reporte
    */
    public function hacerReporte(); 

}