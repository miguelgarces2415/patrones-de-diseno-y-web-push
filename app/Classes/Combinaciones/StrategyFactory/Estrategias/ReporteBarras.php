<?php

namespace App\Classes\Combinaciones\StrategyFactory\Estrategias;

use App\Classes\Combinaciones\StrategyFactory\Interfaces\EstrategiasInterface;

class ReporteBarras implements EstrategiasInterface {
    private $datos;

    public function __construct($datos) {
        $this->datos = $datos;
    }

    public function hacerReporte() {
        echo "Realizar una grafica de barra con los Datos \n";
    }
}