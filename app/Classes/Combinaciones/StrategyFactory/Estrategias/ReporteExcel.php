<?php

namespace App\Classes\Combinaciones\StrategyFactory\Estrategias;

use App\Classes\Combinaciones\StrategyFactory\Interfaces\EstrategiasInterface;

class ReporteExcel implements EstrategiasInterface {
    private $datos;

    public function __construct($datos) {
        $this->datos = $datos;
    }

    public function hacerReporte() {
        echo "Realizar un archivo excel con los Datos \n";
    }
}