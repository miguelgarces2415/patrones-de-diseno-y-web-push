<?php

namespace App\Classes\Combinaciones\StrategyFactory\Factories;

use App\Classes\Combinaciones\StrategyFactory\Estrategias\ReporteBarras;
use App\Classes\Combinaciones\StrategyFactory\Estrategias\ReporteExcel;
use App\Classes\Combinaciones\StrategyFactory\Estrategias\ReporteTorta;

class EstrategiaFactory {

    public static function getReporteEstrategia(array $datos){
        if(isset($datos['celdas'])){
            return new ReporteExcel($datos);
        }else if(isset($datos['torta'])){
            return new ReporteTorta($datos);
        }else if(isset($datos['barras'])){
            return new ReporteBarras($datos);
        }
        return null;
    }

}
