<?php

namespace App\Classes\Patrones\Singleton\Ejemplo1;

class PaqueteMortadelas {

    private static $instance; // Variable para manejar la instancia
    private $mortadelas = 100;

    private function __construct() // Constructor privado para evitar que instancien la clase desde afuera
    {
        echo 'Contruyendo objeto..'.PHP_EOL;
    }

    public static function getInstance() // Creamos una funcion que haga el papel de constructor
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self(); // Generamos la instancia solo si no existia una previamente
        }
        return self::$instance; // Retornamos la $instance que tiene la nueva o anterior instancia
    }

    public function getMortadela()
    {
        $this->mortadelas--;
        echo 'Se toma 1 mortadela quedan: '.$this->mortadelas.'.'.PHP_EOL;
    }

    public function getCantidadMortadelas()
    {
        return $this->mortadelas;
    }
}