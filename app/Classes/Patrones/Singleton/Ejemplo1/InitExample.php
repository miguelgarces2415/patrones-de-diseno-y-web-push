<?php
namespace App\Classes\Patrones\Singleton\Ejemplo1;

use App\Classes\Patrones\Singleton\Ejemplo1\PaqueteMortadelas;

class InitExample{

    public function __construct(){
        $this->init();
    }

    public function init(): void{
        
        PaqueteMortadelas::getInstance()->getMortadela();
        PaqueteMortadelas::getInstance()->getMortadela();
        PaqueteMortadelas::getInstance()->getMortadela();

        echo 'Mortadelas restantes del paquete = '.PaqueteMortadelas::getInstance()->getCantidadMortadelas().PHP_EOL;

    }
}