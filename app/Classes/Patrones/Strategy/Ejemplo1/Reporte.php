<?php

namespace App\Classes\Patrones\Strategy\Ejemplo1;

use App\Classes\Patrones\Strategy\Ejemplo1\Interfaces\EstrategiasInterface;
/*
* Esta es nuestra clase de contexto
*/
class Reporte {

    private $estrategia;
    private $datos;

    public function __construct($datos) {
        $this->datos = $datos;
    }

    public function setEstrategia(EstrategiasInterface $estrategia) {
        $this->estrategia = $estrategia;
    }

    public function ejecutar() {
        $this->estrategia->hacerReporte();
    }
}