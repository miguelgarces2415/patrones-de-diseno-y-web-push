<?php 

namespace App\Classes\Patrones\Strategy\Ejemplo1\Interfaces;

interface EstrategiasInterface {
    /*
    * Obligamos a que todas las estrategias generen un reporte
    */
    public function hacerReporte(); 

}