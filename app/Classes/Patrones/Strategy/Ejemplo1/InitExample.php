<?php
namespace App\Classes\Patrones\Strategy\Ejemplo1;

use App\Classes\Patrones\Strategy\Ejemplo1\Reporte;
use App\Classes\Patrones\Strategy\Ejemplo1\Estrategias\ReporteBarras;
use App\Classes\Patrones\Strategy\Ejemplo1\Estrategias\ReporteExcel;
use App\Classes\Patrones\Strategy\Ejemplo1\Estrategias\ReporteTorta;

class InitExample {

    public function __construct() {
        $this->init();
    }

    /*
    * Es el cliente quien decide que estrategia utilizar
    */
    public function init(): void {
        $datos = array('pais' => 'Colombia');
        $reporte = new Reporte($datos);
        $reporte->setEstrategia(new ReporteBarras($datos));
        $reporte->ejecutar();

        $reporte->setEstrategia(new ReporteExcel($datos));
        $reporte->ejecutar();

        $reporte->setEstrategia(new ReporteTorta($datos));
        $reporte->ejecutar();
    }
}