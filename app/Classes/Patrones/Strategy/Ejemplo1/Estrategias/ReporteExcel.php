<?php

namespace App\Classes\Patrones\Strategy\Ejemplo1\Estrategias;

use App\Classes\Patrones\Strategy\Ejemplo1\Interfaces\EstrategiasInterface;

class ReporteExcel implements EstrategiasInterface {
    private $datos;

    public function __construct($datos) {
        $this->datos = $datos;
    }

    public function hacerReporte() {
        echo "Realizar un archivo excel con los Datos \n";
    }
}