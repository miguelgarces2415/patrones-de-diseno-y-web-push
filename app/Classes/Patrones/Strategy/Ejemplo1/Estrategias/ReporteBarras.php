<?php

namespace App\Classes\Patrones\Strategy\Ejemplo1\Estrategias;

use App\Classes\Patrones\Strategy\Ejemplo1\Interfaces\EstrategiasInterface;

class ReporteBarras implements EstrategiasInterface {
    private $datos;

    public function __construct($datos) {
        $this->datos = $datos;
    }

    public function hacerReporte() {
        echo "Realizar una grafica de barra con los Datos \n";
    }
}