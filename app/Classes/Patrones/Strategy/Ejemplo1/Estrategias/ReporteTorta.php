<?php

namespace App\Classes\Patrones\Strategy\Ejemplo1\Estrategias;

use App\Classes\Patrones\Strategy\Ejemplo1\Interfaces\EstrategiasInterface;

class ReporteTorta implements EstrategiasInterface {
    private $datos;

    public function __construct($datos) {
        $this->datos = $datos;
    }

    public function hacerReporte() {
        echo "Realizar una grafica de torta con los Datos \n";
    }
}