<?php
namespace App\Classes\Patrones\Decoradores\Ejemplo1\Decoradores;

use App\Classes\Patrones\Decoradores\Ejemplo1\Interfaces\CuentaBancariaInterface;
use App\User;


class BlindajeDecorador extends CuentaDecorador{

    public function __construct(CuentaBancariaInterface $cuentaDecorada){
        parent::__construct($cuentaDecorada);
    }

    public function abrirCuenta(User $cuenta){
        $this->cuentaDecorada->abrirCuenta($cuenta); // Ejecuta cuenta de ahorros
        echo $this->agregarBlindaje($cuenta);
    }

    public function agregarBlindaje($cuenta): String {
        return 'Se agrego blindaje a cuenta \n';
    }
}