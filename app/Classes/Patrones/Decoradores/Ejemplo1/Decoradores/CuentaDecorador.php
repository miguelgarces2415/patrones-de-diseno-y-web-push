<?php
namespace App\Classes\Patrones\Decoradores\Ejemplo1\Decoradores;

use App\Classes\Patrones\Decoradores\Ejemplo1\Interfaces\CuentaBancariaInterface;
use App\User;

/*
Las clases abstractas son clases que no se instancian y sólo pueden ser heredadas,
trasladando así un funcionamiento obligatorio a clases hijas
*/

abstract class CuentaDecorador implements CuentaBancariaInterface{

    protected $cuentaDecorada;

    public function __construct(CuentaBancariaInterface $cuentaDecorada){
        $this->cuentaDecorada = $cuentaDecorada;
    }

    public function abrirCuenta(User $cuenta){
        $this->cuentaDecorada->abrirCuenta($cuenta);
    }
}