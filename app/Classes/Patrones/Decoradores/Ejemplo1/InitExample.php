<?php
namespace App\Classes\Patrones\Decoradores\Ejemplo1;

use App\Classes\Patrones\Decoradores\Ejemplo1\CuentaAhorrosClass;
use App\Classes\Patrones\Decoradores\Ejemplo1\Decoradores\BlindajeDecorador;
use App\User;

class InitExample{

    public function __construct(){
        $this->init();
    }

    public function init(): void{
        //$cuenta = User::find(1);
        $user = new User(); 

        $cuenta = new BlindajeDecorador(new CuentaAhorrosClass());
        $cuenta->abrirCuenta($user);
    }
}