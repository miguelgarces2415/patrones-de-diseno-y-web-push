<?php
namespace App\Classes\Patrones\Decoradores\Ejemplo1\Interfaces;

use App\User;

interface CuentaBancariaInterface{
    /*
    * Obligamos a que una cuenta bancaria necesite una cuenta (Que en este caso la cuenta seria un usuario)
    */
    public function abrirCuenta(User $cuenta);
}