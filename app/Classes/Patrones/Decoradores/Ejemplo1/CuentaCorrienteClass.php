<?php
namespace App\Classes\Patrones\Decoradores\Ejemplo1;

use App\User;

class CuentaCorrienteClass implements Interfaces\CuentaBancariaInterface{
    /*
    * Obligamos a que una cuenta bancaria necesite una cuenta (Que en este caso la cuenta seria un usuario)
    */
    public function abrirCuenta(User $cuenta){
        echo "Se abrio cuenta corriente";
    }
}