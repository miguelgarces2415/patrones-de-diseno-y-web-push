<?php
namespace App\Classes\Patrones\Decoradores\Ejemplo2;

use App\Classes\Patrones\Decoradores\Ejemplo2\Macbook;
use App\Classes\Patrones\Decoradores\Ejemplo2\Decoradores\MacbookPro;
use App\Classes\Patrones\Decoradores\Ejemplo2\Decoradores\WithDisplaySize15;
use App\Classes\Patrones\Decoradores\Ejemplo2\Decoradores\WithDisplaySize17;
use App\User;

class InitExample{

    public function __construct(){
        $this->init();
    }

    public function init(): void{
        $macbook = new Macbook();
        echo 'Una Macbook con pantalla de 13 pulgadas cuesta ' . $macbook->getPrice() . ' euros' . PHP_EOL;

        $macbook15Pulg = new WithDisplaySize15(new Macbook());
        echo 'Una Macbook con pantalla de 15 pulgadas cuesta ' . $macbook15Pulg->getPrice() . ' euros' . PHP_EOL;

        $macbook17Pulg = new WithDisplaySize17(new Macbook());
        echo 'Una Macbook con pantalla de 17 pulgadas cuesta ' . $macbook17Pulg->getPrice() . ' euros' . PHP_EOL;

        $macbookPro = new MacbookPro(new Macbook());
        echo 'Una Macbook Pro con pantalla de 13 pulgadas cuesta ' . $macbookPro->getPrice() . ' euros' . PHP_EOL;

        $macbookPro15Pulg = new WithDisplaySize15(new MacbookPro(new Macbook()));
        echo 'Una Macbook Pro con pantalla de 15 pulgadas cuesta ' . $macbookPro15Pulg->getPrice() . ' euros' . PHP_EOL;

        $macbookPro17Pulg = new WithDisplaySize17(new MacbookPro(new Macbook()));
        echo 'Una Macbook Pro con patanlla de 17 pulgadas cuesta ' . $macbookPro17Pulg->getPrice() . ' euros' . PHP_EOL;
    }
}