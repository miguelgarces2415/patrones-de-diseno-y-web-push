<?php

namespace App\Classes\Patrones\Decoradores\Ejemplo2\Decoradores;

abstract class MacbookDecorator {
    
    protected $price;
    protected $macbook;

    public function __construct($macbook)
    {
        $this->macbook = $macbook;
    }
    public function getPrice()
    {
        return $this->macbook->getPrice() + $this->price;
    }
}