<?php

namespace App\Classes\Patrones\Decoradores\Ejemplo2;

class Macbook
{
    protected $basePrice = 1500;
    
    public function getPrice()
    {
        return $this->basePrice;
    }
}