<?php

namespace App\Classes\Patrones\FactoryMethod\Ejemplo2;
use App\Classes\Patrones\FactoryMethod\Ejemplo2\HamburguesaInterface;
use App\Classes\Patrones\FactoryMethod\Ejemplo2\Hamburguesa;

/*
Esta clase podria ser el modelo hamburguesa de eloquent
*/
class HamburguesaFactory {

    public static function obtenerHamburguesa($tipo): HamburguesaInterface {
        switch ($tipo) {
            case 'vegetariana':
                return new Hamburguesa('Pan Integral', 'Carne de soya', ['lechuga', 'tomate', 'cebolla', 'queso']);
            
            case 'americana':
                return new Hamburguesa('Pan Bimbo', 'Carne de res', ['lechuga', 'tomate', 'cebolla', 'queso']);

            default:
                return null;
        }
    } 
}