<?php 
namespace App\Classes\Patrones\FactoryMethod\Ejemplo2;

use App\Classes\Patrones\FactoryMethod\Ejemplo2\HamburguesaFactory;

/*
* @author Miguel Garces
*/

class InitExample {

    public function __construct(){
        $this->init();
    }
    /*
    * El cliente queda entonces liberado de detalles de construcción de la hamburguesa
    */
    public function init(){

        $vegetariana = HamburguesaFactory::obtenerHamburguesa('vegetariana');
        var_dump('Vegetariana', $vegetariana->toArray());

        $americana = HamburguesaFactory::obtenerHamburguesa('americana');
        var_dump('Americana', $americana->toArray());
    }

}