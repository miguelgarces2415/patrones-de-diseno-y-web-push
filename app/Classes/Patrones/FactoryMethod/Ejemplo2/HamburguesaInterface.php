<?php 

namespace App\Classes\Patrones\FactoryMethod\Ejemplo2;

interface HamburguesaInterface {
    /*
    * Obligamos a que todas las Hamburguesa tengan la funcion requerida
    */
    public function toArray(); 

}