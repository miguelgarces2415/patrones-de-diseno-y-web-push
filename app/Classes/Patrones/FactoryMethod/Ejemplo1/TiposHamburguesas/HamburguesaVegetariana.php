<?php

namespace App\Classes\Patrones\FactoryMethod\Ejemplo1\TiposHamburguesas;
use App\Classes\Patrones\FactoryMethod\Ejemplo1\HamburguesaInterface;
use App\Classes\Patrones\FactoryMethod\Ejemplo1\Hamburguesa;

/*
Esta clase podria ser el modelo hamburguesa de eloquent
*/
class HamburguesaVegetariana extends HamburguesaFactory {

    public function obtenerHamburguesa(): HamburguesaInterface {
        return new Hamburguesa('Pan Integral', 'Carne de soya', ['lechuga', 'tomate', 'cebolla', 'queso']);
    } 
}