<?php

namespace App\Classes\Patrones\FactoryMethod\Ejemplo1\TiposHamburguesas;
use App\Classes\Patrones\FactoryMethod\Ejemplo1\HamburguesaInterface;
use App\Classes\Patrones\FactoryMethod\Ejemplo1\Hamburguesa;

/*
Esta clase podria ser el modelo hamburguesa de eloquent
*/
class HamburguesaAmericana extends HamburguesaFactory {

    public function obtenerHamburguesa(): HamburguesaInterface {
        return new Hamburguesa('Pan Bimbo', 'Carne de Res', ['lechuga', 'tomate', 'cebolla', 'queso']);
    } 
}