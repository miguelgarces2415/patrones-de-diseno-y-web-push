<?php

namespace App\Classes\Patrones\FactoryMethod\Ejemplo1\TiposHamburguesas;
use App\Classes\Patrones\FactoryMethod\Ejemplo1\HamburguesaInterface;

/*
*/
abstract class HamburguesaFactory {

    abstract public function obtenerHamburguesa(): HamburguesaInterface;
}