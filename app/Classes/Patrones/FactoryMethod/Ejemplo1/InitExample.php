<?php 
namespace App\Classes\Patrones\FactoryMethod\Ejemplo1;

use App\Classes\Patrones\FactoryMethod\Ejemplo1\TiposHamburguesas\HamburguesaAmericana;
use App\Classes\Patrones\FactoryMethod\Ejemplo1\TiposHamburguesas\HamburguesaVegetariana;

/*
* @author Miguel Garces
*/

class InitExample {

    public function __construct(){
        $this->init();
    }
    /*
    * El cliente queda entonces liberado de detalles de construcción de la hamburguesa
    */
    public function init(){

        $hamburgesa = new HamburguesaVegetariana();
        $vegetariana = $hamburgesa->obtenerHamburguesa();
        var_dump('Vegetariana', $vegetariana->toArray());
        
        $hamburgesa = new HamburguesaAmericana();
        $americana = $hamburgesa->obtenerHamburguesa();
        var_dump('Americana', $americana->toArray());
    }

}