<?php

namespace App\Classes\Patrones\Builder\Ejemplo1;

/*
Esta clase podria ser el modelo hamburguesa de eloquent
*/
class Hamburguesa {

    private $proteina;
    private $toppings = [];
    private $pan;

    public function setPan(string $pan): void {
        $this->pan = $pan;
    }

    public function setProteina(string $proteina): void {
        $this->proteina = $proteina;
    }

    public function addToppings(array $toppings): void {
        $this->toppings = $toppings;
    }

    public function toArray(): array{
        $result = [
            $this->pan,
            $this->proteina
        ];
        $result = array_merge($result, $this->toppings);
        $result[] = $this->pan;
        return $result;
    }
}