<?php 
namespace App\Classes\Patrones\Builder\Ejemplo1;

use App\Classes\Patrones\Builder\Ejemplo1\ChefHamburguesas;
use App\Classes\Patrones\Builder\Ejemplo1\Builders\HamburguesaAmericanaBuilder;
use App\Classes\Patrones\Builder\Ejemplo1\Builders\HamburguesaVegetarianaBuilder;

/*
* @author Miguel Garces
*/

class InitExample {

    public function __construct(){
        $this->init();
    }
    /*
    * El cliente queda entonces liberado de detalles de construcción de la hamburguesa
    */
    public function init(){

        $chef = new ChefHamburguesas();
        $vegetariana = $chef->fabricarHamburguesa(new HamburguesaVegetarianaBuilder());
        var_dump('Vegetariana', $vegetariana->toArray());
        $americana = $chef->fabricarHamburguesa(new HamburguesaAmericanaBuilder());
        var_dump('Americana', $americana->toArray());
    }

}