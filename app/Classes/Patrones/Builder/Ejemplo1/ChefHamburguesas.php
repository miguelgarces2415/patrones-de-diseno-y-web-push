<?php

namespace App\Classes\Patrones\Builder\Ejemplo1;

use App\Classes\Patrones\Builder\Ejemplo1\Hamburguesa;
use App\Classes\Patrones\Builder\Ejemplo1\Template\HamburguesaBuilder;

/*
* El director, es decir, el chef de hamburguesas, controla y gestiona de forma 
* precisa el proceso de creación de la hamburguesa
*/

class ChefHamburguesas {

    public function fabricarHamburguesa(HamburguesaBuilder $builder): Hamburguesa {

        $builder->createHamburguesa();
        $builder->preparePan();
        $builder->azarProteina();
        $builder->ponerToppings();

        return $builder->getHamburguesa();
    }
}