<?php

namespace App\Classes\Patrones\Builder\Ejemplo1\Builders;

use App\Classes\Patrones\Builder\Ejemplo1\Template\HamburguesaBuilder;

class HamburguesaVegetarianaBuilder extends HamburguesaBuilder {

    public function preparePan(): void {
        $this->hamburguesa->setPan('Pan Integral');
    }

    public function azarProteina(): void {
        $this->hamburguesa->setProteina('Carne de soya');
    }

    public function ponerToppings(): void {
        $this->hamburguesa->addToppings(['lechuga', 'tomate', 'cebolla', 'queso']);
    }
}