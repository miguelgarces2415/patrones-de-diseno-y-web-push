<?php

namespace App\Classes\Patrones\Builder\Ejemplo1\Template;

use App\Classes\Patrones\Builder\Ejemplo1\Hamburguesa;

/*
En esta clase hacemos uso del patron de diseño: Template Method, en el cual definimos 
el esqueleto de un algoritmo en una clase base y permite a las subclases sobrescribir 
los pasos sin cambiar la estructura general del algoritmo.

Ejemplo: La plantilla para preparar hamburguesas, debe ser siempre: Preparar el pan, azar la proteina y agregar los toppings
Nota: Un template se puede crear apartir de funciones abstractas en la clase abstracta padre, pero
tambien se podria crear implementando una interface en la clase abstracta padre, ya que el objetivo del patron de diseño
es obligar a las hijas a conservar la estrutura general del algoritmo.

Diferencias entre clase Abstract y Interface:
Interface: Son clases vacías que esperan que las clases secundarias implementen todo para ellas,
por ende solo sirven para imponer una regla de estructura sobre las secundarias (Las que implementan la interface)
Nota: En estas clases no deberia ir logica de funciones ni un constructor
Class Abstract: Son clases que no solo puede contener información común implementándose dentro de ellos, 
sino que también puede esperar que las clases secundarias llenen los espacios restantes + obligar a que tengan ciertas funciones
declaradas como abstractas para sus hijas (Regla por heredacion)
Nota: En estas clases si puede ir logica en funciones , incluso un constructor, pero las funciones que se marquen como
abstract no deben tener logica , ya que se piensan llenar desde las hijas
*/
abstract class HamburguesaBuilder {

    protected $hamburguesa;

    public function createHamburguesa(): void {
        $this->hamburguesa = new Hamburguesa();
    }

    public function getHamburguesa(): Hamburguesa {
        return $this->hamburguesa;
    }

    abstract public function preparePan(): void;
    abstract public function azarProteina(): void;
    abstract public function ponerToppings(): void;
}