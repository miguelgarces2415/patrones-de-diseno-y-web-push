<?php

namespace App\Classes\Patrones\Builder\Ejemplo2;

use App\Classes\Patrones\Builder\Ejemplo2\Builders\HamburguesaBuilder;

/*
Esta clase podria ser el modelo Builder de eloquent
*/
class Hamburguesa {

    protected $proteina;
    protected $toppings = [];
    protected $pan;

    public function __construct(HamburguesaBuilder $hamburguesa) {
        $this->proteina = $hamburguesa->proteina;
        $this->toppings = $hamburguesa->toppings;
        $this->pan = $hamburguesa->pan;
    }

    public function toArray(): array{
        $result = [
            $this->pan,
            $this->proteina
        ];
        $result = array_merge($result, $this->toppings);
        $result[] = $this->pan;
        return $result;
    }
}