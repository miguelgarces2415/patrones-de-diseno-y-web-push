<?php

namespace App\Classes\Patrones\Builder\Ejemplo2\Builders;

use App\Classes\Patrones\Builder\Ejemplo2\Hamburguesa;
use App\Classes\Patrones\Builder\Ejemplo2\Interfaces\HamburguesaBuilderInterface;

class HamburguesaBuilder implements HamburguesaBuilderInterface {

    public $pan = '';
    public $proteina = '';
    public $toppings = [];

    public function __construct() {
    }

    public function preparePan() {
        $this->pan = 'Pan Bimbo';
        return $this;
    }

    public function azarProteina() {
        $this->proteina = 'Carne de Res';
        return $this;
    }

    public function ponerToppings() {
        $this->toppings = ['lechuga', 'tomate', 'cebolla', 'queso'];
        return $this;
    }

    public function build(): Hamburguesa {
        return new Hamburguesa($this);
    }
}