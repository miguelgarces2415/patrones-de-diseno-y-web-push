<?php

namespace App\Classes\Patrones\Builder\Ejemplo2;

use App\Classes\Patrones\Builder\Ejemplo2\Builders\HamburguesaBuilder;

/*
* El director, es decir, el chef de hamburguesas, controla y gestiona de forma 
* precisa el proceso de creación de la hamburguesa
*/

class ChefHamburguesas {

    public function fabricarHamburguesa() {
        return (new HamburguesaBuilder())
                    ->preparePan()
                    ->azarProteina()
                    ->ponerToppings()
                    ->build();
    }
}