<?php 
namespace App\Classes\Patrones\Builder\Ejemplo2;

use App\Classes\Patrones\Builder\Ejemplo2\ChefHamburguesas;

/*
* @author Miguel Garces
*/

class InitExample {

    public function __construct(){
        $this->init();
    }
    /*
    * El cliente queda entonces liberado de detalles de construcción de la hamburguesa
    */
    public function init(){

        $chef = new ChefHamburguesas();
        $hamburguesa = $chef->fabricarHamburguesa();
        var_dump('Hamburguesa', $hamburguesa->toArray());
    }

}