<?php
namespace App\Classes\Patrones\Builder\Ejemplo2\Interfaces;


interface HamburguesaBuilderInterface{
    /*
    * Obligamos a que una cuenta bancaria necesite una cuenta (Que en este caso la cuenta seria un usuario)
    */
    public function preparePan();
    public function azarProteina();
    public function ponerToppings();
    public function build();
}