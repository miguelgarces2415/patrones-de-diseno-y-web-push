<?php 
namespace App\Classes\Patrones\FactoryAbstract\Ejemplo2;

use App\Classes\Patrones\FactoryAbstract\Ejemplo2\Fabricas\HamburguesaCombo1Factory;
use App\Classes\Patrones\FactoryAbstract\Ejemplo2\Fabricas\HamburguesaCombo2Factory;

/*
* @author Miguel Garces
*/

class InitExample {

    public function __construct(){
        $this->init();
    }
    /*
    * El cliente queda entonces liberado de detalles de construcción de la hamburguesa
    */
    public function init(){

        $combo = new HamburguesaCombo1Factory();

        $hamburguesa = $combo->crearHamburguesa();
        $acompanante = $combo->crearAcompanante();

        dump('Combo 1 hamburguesa', $hamburguesa->toArray(), 'Acompañante: '.$acompanante->getAcompanante());
        dump('*********************************');
        $combo = new HamburguesaCombo2Factory();
        
        $hamburguesa = $combo->crearHamburguesa();
        $acompanante = $combo->crearAcompanante();
        
        dump('Combo 2 hamburguesa', $hamburguesa->toArray(), 'Acompañante: '.$acompanante->getAcompanante());
        
    }

}