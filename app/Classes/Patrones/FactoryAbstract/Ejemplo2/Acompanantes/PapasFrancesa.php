<?php

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo2\Acompanantes;


class PapasFrancesa implements PapasInterface{

    private $acompanante = 'Papas francesa';

    public function __construct() {

    }

    public function getAcompanante(): string {
        return $this->acompanante;
    }
}