<?php

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo2\Acompanantes;


class PapasCasco implements PapasInterface{

    private $acompanante = 'Papas casco';

    public function __construct() {

    }

    public function getAcompanante(): string {
        return $this->acompanante;
    }
}