<?php 

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo2\Acompanantes;

interface PapasInterface {

    public function getAcompanante(); 

}