<?php 

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo2\Fabricas;

use App\Classes\Patrones\FactoryAbstract\Ejemplo2\Acompanantes\PapasInterface;
use App\Classes\Patrones\FactoryAbstract\Ejemplo2\Productos\HamburguesaInterface;

interface HamburguesaCombosInterface {

    public function crearHamburguesa(): HamburguesaInterface; 
    public function crearAcompanante(): PapasInterface; 

}