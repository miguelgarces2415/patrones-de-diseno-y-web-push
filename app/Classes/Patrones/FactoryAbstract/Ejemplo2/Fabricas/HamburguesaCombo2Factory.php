<?php 

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo2\Fabricas;

use App\Classes\Patrones\FactoryAbstract\Ejemplo2\Acompanantes\PapasCasco as Papas;
use App\Classes\Patrones\FactoryAbstract\Ejemplo2\Productos\HamburguesaCarne as Hamburguesa;

use App\Classes\Patrones\FactoryAbstract\Ejemplo2\Productos\HamburguesaInterface;
use App\Classes\Patrones\FactoryAbstract\Ejemplo2\Acompanantes\PapasInterface;

class HamburguesaCombo2Factory implements HamburguesaCombosInterface {

    public function crearHamburguesa(): HamburguesaInterface 
    {
        return new Hamburguesa('Pan Bimbo', ['lechuga', 'tomate', 'cebolla', 'queso']);
    } 

    public function crearAcompanante(): PapasInterface
    {
        return new Papas();
    } 

}