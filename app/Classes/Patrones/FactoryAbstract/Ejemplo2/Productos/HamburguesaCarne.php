<?php

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo2\Productos;


class HamburguesaCarne implements HamburguesaInterface{

    private $proteina = 'Carne de Res';
    private $toppings = [];
    private $pan;

    public function __construct(string $pan, array $toppings) {
        $this->pan = $pan;
        $this->toppings = $toppings;
    }

    public function toArray(): array{
        $result = [
            $this->pan,
            $this->proteina
        ];
        $result = array_merge($result, $this->toppings);
        $result[] = $this->pan;
        return $result;
    }
}