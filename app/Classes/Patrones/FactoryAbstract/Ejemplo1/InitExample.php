<?php 
namespace App\Classes\Patrones\FactoryAbstract\Ejemplo1;

use App\Classes\Patrones\FactoryAbstract\Ejemplo1\Fabricas\PuertasMaderaFactory;
use App\Classes\Patrones\FactoryAbstract\Ejemplo1\Fabricas\PuertasMetalicasFactory;

/*
* @author Miguel Garces
*/

class InitExample {

    public function __construct(){
        $this->init();
    }
    /*
    * El cliente queda entonces liberado de detalles de construcción de la hamburguesa
    */
    public function init(){

        $maderaFactory = new PuertasMaderaFactory();

        $puerta = $maderaFactory->crearPuerta();
        $experto = $maderaFactory->crearExpertoPuertas();

        $puerta->getDescription();
        $experto->getDescription();

        $metalicaFactory = new PuertasMetalicasFactory();

        $puerta = $metalicaFactory->crearPuerta();
        $experto = $metalicaFactory->crearExpertoPuertas();

        $puerta->getDescription();
        $experto->getDescription();
        
    }

}