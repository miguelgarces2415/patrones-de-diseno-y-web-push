<?php 

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo1\Puertas;

interface PuertasInterface {
    /*
    * Obligamos a que todas las puertas tengan descripción
    */
    public function getDescription(); 

}