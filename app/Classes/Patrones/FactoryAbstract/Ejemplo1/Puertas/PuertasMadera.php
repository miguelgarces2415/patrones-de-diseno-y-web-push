<?php

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo1\Puertas;


class PuertasMadera implements PuertasInterface{

    public function getDescription()
    {
        echo 'Soy una puerta de madera'.PHP_EOL;
    }
}