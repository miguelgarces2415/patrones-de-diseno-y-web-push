<?php

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo1\Puertas;


class PuertasMetalicas implements PuertasInterface{

    public function getDescription()
    {
        echo 'Soy una puerta metalica'.PHP_EOL;
    }
}