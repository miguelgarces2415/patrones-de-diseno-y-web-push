<?php 

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo1\Fabricas;

use App\Classes\Patrones\FactoryAbstract\Ejemplo1\Puertas\PuertasMadera;
use App\Classes\Patrones\FactoryAbstract\Ejemplo1\ExpertosPuertas\Carpintero;

use App\Classes\Patrones\FactoryAbstract\Ejemplo1\Puertas\PuertasInterface;
use App\Classes\Patrones\FactoryAbstract\Ejemplo1\ExpertosPuertas\ExpertosPuertasInterface;

/*
Esta clase encapsula la creacion de puertas de maderas y carpintero
*/
class PuertasMaderaFactory implements PuertasFactoryInterface {

    public function crearPuerta(): PuertasInterface 
    {
        return new PuertasMadera();
    } 

    public function crearExpertoPuertas(): ExpertosPuertasInterface
    {
        return new Carpintero();
    } 

}