<?php 

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo1\Fabricas;

use App\Classes\Patrones\FactoryAbstract\Ejemplo1\Puertas\PuertasInterface;
use App\Classes\Patrones\FactoryAbstract\Ejemplo1\ExpertosPuertas\ExpertosPuertasInterface;

interface PuertasFactoryInterface {

    public function crearPuerta(): PuertasInterface; 
    public function crearExpertoPuertas(): ExpertosPuertasInterface; 

}