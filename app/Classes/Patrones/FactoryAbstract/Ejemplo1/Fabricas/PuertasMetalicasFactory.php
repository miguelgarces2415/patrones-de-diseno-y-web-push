<?php 

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo1\Fabricas;

use App\Classes\Patrones\FactoryAbstract\Ejemplo1\Puertas\PuertasMetalicas;
use App\Classes\Patrones\FactoryAbstract\Ejemplo1\ExpertosPuertas\Soldador;

use App\Classes\Patrones\FactoryAbstract\Ejemplo1\Puertas\PuertasInterface;
use App\Classes\Patrones\FactoryAbstract\Ejemplo1\ExpertosPuertas\ExpertosPuertasInterface;

/*
Esta clase encapsula la creacion de puertas metalicas y soldador
*/
class PuertasMetalicasFactory implements PuertasFactoryInterface {

    public function crearPuerta(): PuertasInterface 
    {
        return new PuertasMetalicas();
    } 
    
    public function crearExpertoPuertas(): ExpertosPuertasInterface
    {
        return new Soldador();
    } 

}