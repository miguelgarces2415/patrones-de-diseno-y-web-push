<?php

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo1\ExpertosPuertas;


class Soldador implements ExpertosPuertasInterface {

    public function getDescription()
    {
        echo 'Solo coloco puertas metalicas'.PHP_EOL;
    }
}