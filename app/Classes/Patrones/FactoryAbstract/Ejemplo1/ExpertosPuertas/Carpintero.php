<?php

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo1\ExpertosPuertas;


class Carpintero implements ExpertosPuertasInterface {

    public function getDescription()
    {
        echo 'Solo coloco puertas de madera'.PHP_EOL;
    }
}