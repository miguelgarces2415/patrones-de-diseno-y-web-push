<?php 

namespace App\Classes\Patrones\FactoryAbstract\Ejemplo1\ExpertosPuertas;

interface ExpertosPuertasInterface {

    public function getDescription(); 

}