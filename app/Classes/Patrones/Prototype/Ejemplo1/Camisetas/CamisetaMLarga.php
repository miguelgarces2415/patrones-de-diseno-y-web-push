<?php
namespace App\Classes\Patrones\Prototype\Ejemplo1\Camisetas;


class CamisetaMLarga extends Camiseta{

    public function __construct(int $talla, string $color, string $estampado){
        $this->nombre = 'Prototipo camiseta larga';
        $this->talla = $talla;
        $this->color = $color;
        $this->manga = 'Larga';
        $this->estampado = $estampado;
        $this->material = 'Lana';
    }

    public function setTalla(int $talla){
        $this->talla = $talla;
    }

    public function __clone(){
        return new CamisetaMLarga($this->talla, $this->color, $this->estampado);
    }

}