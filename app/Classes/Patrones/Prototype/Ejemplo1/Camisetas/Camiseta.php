<?php
namespace App\Classes\Patrones\Prototype\Ejemplo1\Camisetas;


abstract class Camiseta{

    private $nombre;
    private $talla;
    private $color;
    private $manga;
    private $estampado;
    private $material;

    public function setTalla(int $talla){
        $this->talla = $talla;
    }

    abstract public function __clone();

}