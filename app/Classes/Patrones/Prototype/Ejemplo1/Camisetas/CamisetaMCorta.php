<?php
namespace App\Classes\Patrones\Prototype\Ejemplo1\Camisetas;


class CamisetaMCorta extends Camiseta{

    public function __construct(int $talla, string $color, string $estampado){
        $this->nombre = 'Prototipo camiseta corta';
        $this->talla = $talla;
        $this->color = $color;
        $this->manga = 'Corta';
        $this->estampado = $estampado;
        $this->material = 'Lana';
    }

    public function setTalla(int $talla){
        $this->talla = $talla;
    }

    public function __clone(){
        return new CamisetaMCorta($this->talla, $this->color, $this->estampado);
    }

}