<?php
namespace App\Classes\Patrones\Prototype\Ejemplo1;

use App\Classes\Patrones\Prototype\Ejemplo1\Camisetas\CamisetaMCorta;
use App\Classes\Patrones\Prototype\Ejemplo1\Camisetas\CamisetaMLarga;

class InitExample{

    public function __construct(){
        $this->init();
    }

    public function init(): void{
        
        $camiseta_corta = new CamisetaMCorta(40, 'blanco', 'Logo');
        $camiseta_larga = new CamisetaMLarga(40, 'blanco', 'Logo');

        $inventario = [];
        
        $tallas = [30, 40, 50, 20];

        for ($i=0; $i < 10; $i++) { 
            shuffle($tallas);
            $copia_corta = clone $camiseta_corta;
            $copia_corta->setTalla($tallas[0]);
            $inventario[] = $copia_corta;

            $copia_larga = clone $camiseta_larga;
            $copia_larga->setTalla($tallas[0]);
            $inventario[] = $copia_larga;
        }
        
        dd("Camisetas creadas: ", $inventario);
    }
}