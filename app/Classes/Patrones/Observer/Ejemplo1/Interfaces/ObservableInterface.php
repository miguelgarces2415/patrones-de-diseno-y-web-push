<?php
namespace App\Classes\Patrones\Observer\Ejemplo1\Interfaces;

use App\Classes\Patrones\Observer\Ejemplo1\Trabajos\Trabajo;
use App\Classes\Patrones\Observer\Ejemplo1\Interfaces\ObservadorInterface;

interface ObservableInterface{

    public function notify(Trabajo $trabajo);

    public function attach(ObservadorInterface $observador);

    public function dettach(ObservadorInterface $observador);
}