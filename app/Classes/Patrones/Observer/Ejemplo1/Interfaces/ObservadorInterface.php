<?php
namespace App\Classes\Patrones\Observer\Ejemplo1\Interfaces;

use App\Classes\Patrones\Observer\Ejemplo1\Trabajos\Trabajo;

interface ObservadorInterface{
    /*
    * Obligamos a que una cuenta bancaria necesite una cuenta (Que en este caso la cuenta seria un usuario)
    */
    public function nuevoTrabajo(Trabajo $trabajo);
}