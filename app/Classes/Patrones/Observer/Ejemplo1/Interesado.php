<?php
namespace App\Classes\Patrones\Observer\Ejemplo1;

use App\Classes\Patrones\Observer\Ejemplo1\Interfaces\ObservadorInterface;
use App\Classes\Patrones\Observer\Ejemplo1\Trabajos\Trabajo;

class Interesado implements ObservadorInterface {

    protected $name;

    public function __construct(string $name){
        $this->name = $name;
    }

    public function nuevoTrabajo(Trabajo $trabajo){
        echo "Hola: ".$this->name." hay un nuevo trabajo disponible: ".$trabajo->getTitle().PHP_EOL;
    }
}