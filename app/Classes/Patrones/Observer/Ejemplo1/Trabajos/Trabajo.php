<?php
namespace App\Classes\Patrones\Observer\Ejemplo1\Trabajos;


class RepresentacionMiembroConsorcio {

    protected $jefe;
    protected $idProponente;
    protected $porcentaje;

    public function __construct(string $title, $idProponente, $porcentaje){
        $this->title = $title;
    }

    public function getTitle(){
        return $this->title;
    }
    
    public function toArray(){}
}