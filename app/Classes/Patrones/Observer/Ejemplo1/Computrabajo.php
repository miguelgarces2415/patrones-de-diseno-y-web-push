<?php
namespace App\Classes\Patrones\Observer\Ejemplo1;

use App\Classes\Patrones\Observer\Ejemplo1\Interfaces\ObservableInterface;
use App\Classes\Patrones\Observer\Ejemplo1\Interfaces\ObservadorInterface;
use App\Classes\Patrones\Observer\Ejemplo1\Trabajos\Trabajo;

class Computrabajo implements ObservableInterface {

    protected $observadores = [];

    public function notify(Trabajo $trabajo){
        foreach ($this->observadores as $observador) {
            $observador->nuevoTrabajo($trabajo);
        }
    }

    public function attach(ObservadorInterface $interesado){
        $this->observadores[] = $interesado;
    }
    
    public function dettach(ObservadorInterface $interesado){
        //$this->observadores[] = $interesado;
    }

    public function agregarTrabajo(Trabajo $trabajo){
        $this->notify($trabajo);
    }
}