<?php
namespace App\Classes\Patrones\Observer\Ejemplo1;

use App\Classes\Patrones\Observer\Ejemplo1\Computrabajo;
use App\Classes\Patrones\Observer\Ejemplo1\Interesado;
use App\Classes\Patrones\Observer\Ejemplo1\Trabajos\Trabajo;

class InitExample{

    public function __construct(){
        $this->init();
    }

    public function init(): void{
        // Los interesados en ver nuevos trabajos
        $oscar = new Interesado('Oscar');
        $andres = new Interesado('Andres');

        // Sitio donde se publican los trabajos
        $computrabajo = new Computrabajo();
        $computrabajo->attach($oscar);
        $computrabajo->attach($andres);

        // Agregar nuevo trabajo
        $computrabajo->agregarTrabajo(new Trabajo('Desarrollador Laravel'));

    }
}