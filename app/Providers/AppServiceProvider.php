<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Classes\Patrones\Singleton\Ejemplo1\PaqueteMortadelas;
use Illuminate\Support\Facades\Response;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::mixin(new \App\Mixins\ResponseMixin);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('PaqueteMortadelas', function ($app) {
            return PaqueteMortadelas::getInstance();
        });
    }
}
