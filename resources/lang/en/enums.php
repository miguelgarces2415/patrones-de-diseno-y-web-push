<?php

use App\Enums\DesayunoMarzoType;

return [
    DesayunoMarzoType::class => [
        DesayunoMarzoType::Sushi => 'Sushi de sarkujapan',
        DesayunoMarzoType::Creeps => 'Creep de banano',
        DesayunoMarzoType::HuevoyArepa => 'Huevo y arepa tradicional',
    ],
];